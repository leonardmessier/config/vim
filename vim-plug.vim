""""""""""""
" VIM-PLUG "
""""""""""""

call plug#begin()
" UI
" --

Plug 'altercation/vim-colors-solarized'
Plug 'ryanoasis/vim-devicons'


" GIT
" ---

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/gv.vim'
Plug 'rhysd/git-messenger.vim'
Plug 'shumphrey/fugitive-gitlab.vim'


" VIM
" ---

Plug 'chamindra/marvim'


" CODE
" ----

Plug 'dense-analysis/ale'
Plug 'AndrewRadev/sideways.vim'
Plug 'AndrewRadev/deleft.vim'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'AndrewRadev/linediff.vim'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-abolish'
Plug 'kurkale6ka/vim-swap'
Plug 'alvan/vim-closetag'
Plug 'zef/vim-cycle'
Plug 'matze/vim-move'
Plug 'jsborjesson/vim-uppercase-sql'
Plug 'godlygeek/tabular'
Plug 'editorconfig/editorconfig-vim'
Plug 'ddollar/nerdcommenter'
Plug 'SirVer/ultisnips'
Plug 'ervandew/supertab'
Plug 'mattn/emmet-vim'
Plug 'dhruvasagar/vim-table-mode'
Plug 'simnalamburt/vim-mundo'

Plug 'vim-vdebug/vdebug'
Plug 'janko-m/vim-test'


" TEXT OBJECTS
" ------------

Plug 'kana/vim-textobj-function'
Plug 'kana/vim-textobj-user'
Plug 'Julian/vim-textobj-brace'
Plug 'Julian/vim-textobj-variable-segment'
Plug 'sgur/vim-textobj-parameter'
Plug 'saaguero/vim-textobj-pastedtext'
Plug 'whatyouhide/vim-textobj-xmlattr'


" NAVIGATION
" ---------

Plug 'tpope/vim-obsession'
Plug 'tpope/vim-projectionist'
Plug 'justinmk/vim-gtfo'
Plug 'airblade/vim-rooter'
Plug 'dpwright/vim-gf-ext'
Plug 'MattesGroeger/vim-bookmarks'


" FILE TYPE SPECIFIC
" ------------------

Plug 'tpope/vim-obsession'
Plug 'lumiliet/vim-twig'
Plug 'tpope/vim-bundler'
Plug 'tpope/vim-cucumber'
Plug 'tpope/vim-jdaddy'
Plug 'tpope/vim-rails'
Plug 'tpope/vim-rake'
Plug 'ain/vim-docker'
Plug 'markcornick/vim-bats'

Plug 'mustache/vim-mustache-handlebars'
Plug 'noahfrederick/vim-composer'
Plug 'leafgarland/typescript-vim'
Plug 'aklt/plantuml-syntax'
Plug 'posva/vim-vue'
Plug 'adborden/vim-notmuch-address'

Plug 'embear/vim-localvimrc'
Plug 'tpope/vim-dispatch'

Plug 'aeke/vim-php-cs-fixer'
Plug 'shime/vim-livedown'
Plug 'diepm/vim-rest-console'


" SEARCH
" ------

Plug 'jremmen/vim-ripgrep'


" DOC
" ---

Plug 'sunaku/vim-dasht'


" UI
" --

Plug 'vim-airline/vim-airline'
Plug 'mhinz/vim-startify'
Plug 'junegunn/fzf.vim'
Plug 'junegunn/fzf'
Plug 'voldikss/vim-floaterm'
Plug 'yssl/QFEnter'
Plug 'rafaqz/ranger.vim'
Plug 'mtth/scratch.vim'


" ENVIRONMENT
" -----------

Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'direnv/direnv.vim'


" COMPLETE
" --------

Plug 'phpactor/phpactor', {'for': 'php', 'tag': '*', 'do': 'composer install --no-dev -o'}
Plug 'neoclide/coc.nvim', {'branch': 'release'}


" DATABASE
" --------

Plug 'tpope/vim-dadbod'
Plug 'kristijanhusak/vim-dadbod-ui'
Plug 'kristijanhusak/vim-dadbod-completion'


" MISC
" ----

Plug 'johngrib/vim-game-code-break'
call plug#end()
