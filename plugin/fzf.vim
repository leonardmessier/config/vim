let $FZF_DEFAULT_OPTS="--preview-window 'right:57%' --preview 'bat --style=number --line-range :300 {}'
\ --bind ctrl-k:preview-up,ctrl-j:preview-down,
\ctrl-u:preview-page-up,ctrl-d:preview-page-down,
\ctrl-g:preview-top,ctrl-G:preview-bottom"

" → Searching for files in folder with .git
" as a root reference if it exists
nnoremap <leader>pf :Files<CR>

" → Searching in buffers
nnoremap <leader>pb :History<CR>

" → searching in command history
nnoremap <leader>ph :History:<CR>

" → Searching in command history
nnoremap <leader>ps :History/<CR>

" → Searching git files
nnoremap <leader>pgf :GFiles<CR>

" → Searching in modified files
nnoremap <leader>pgm :GFiles?<CR>

" → Searching in commands
nnoremap <leader>pc :Commands<CR>

" → Searching in commits
nnoremap <leader>pgc :Commits<CR>

" → Searching in commits for current buffer
nnoremap <leader>pgb :BCommits<CR>

" → Filtering search results
nnoremap <leader>pgr :Rg
