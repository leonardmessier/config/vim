let g:floaterm_opener = 'edit'

nmap - :FloatermNew --height=0.8 --width=0.8 --title=Ranger --titleposition=center ranger<CR>