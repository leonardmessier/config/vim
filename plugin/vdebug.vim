let g:vdebug_keymap = {
\    "run" : "<Leader>vr",
\    "run_to_cursor" : "<Leader>vrc",
\    "step_over" : "<Leader>vso",
\    "step_into" : "<Leader>vsi",
\    "step_out" : "<Leader>vst",
\    "close" : "<Leader>vc",
\    "detach" : "<Leader>vd",
\    "set_breakpoint" : "<Leader>vb",
\    "get_context" : "<Leader>vg",
\    "eval_under_cursor" : "<Leader>ve",
\    "eval_visual" : "<Leader>vve"
\}

if !empty($VDEBUG)
  let g:vdebug_options = {
        \ 'server': '0.0.0.0',
        \ 'port': $VDEBUG_PORT,
        \ 'debug_file': '~/vdebug.log',
        \ 'debug_file_level': 2,
        \ 'break_on_open': 0,
        \ 'on_close': 'detach',
        \ 'path_maps': {
        \ $VDEBUG_GUEST_PATH: $VDEBUG_HOST_PATH
        \}
        \}

  let g:vdebug_features = {
      \ 'max_depth': 1,
      \ 'max_data': 1024,
      \ 'max_children': 100
      \}
endif
