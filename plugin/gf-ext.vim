if !isdirectory($VIMRUNTIME . 'plugged/gf-ext')
    finish
endif

if empty(glob("~/.vim/vimrc.init"))
  if has("autocmd") && (match(system('uname -s'), 'Darwin') < 0)
    call gf_ext#add_handler('\.pdf$', "!evince")
  endif
endif

