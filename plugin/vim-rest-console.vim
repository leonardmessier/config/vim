let g:vrc_auto_format_response_patterns = {
\   'json': 'json_reformat',
\   'xml': 'xmllint --format -',
\}

if !empty($VRC_SHOW_COMMAND)
  let g:vrc_show_command = 1
endif
