if isdirectory('environments')
  autocmd BufWritePost environments/*.json silent :DirenvExport
endif

function JqfySwitch(env)
  call system('jqfy switch ' . a:env)
  DirenvExport
  call system('direnv allow')
  checktime
endfunction

function JqfyEdit(env)
  execute 'edit ' . 'environments/' .a:env. '.json'
endfunction

function JqfySet(env, key, value)
  echom 'jqfy set ' . a:env . ' ' . a:key . ' ' . a:value
  call system('jqfy set ' . a:env . ' ' . a:key . ' ' . a:value)
  DirenvExport
  call system('direnv allow')
  checktime
endfunction

:command! -nargs=1 JqfySwitch :call JqfySwitch(<q-args>)
:command! -nargs=1 JqfyEdit :call JqfyEdit(<q-args>)
:command! -nargs=* JqfySet :call JqfySet(<f-args>)
