if !isdirectory($VIMRUNTIME . 'plugged/vim-ripgrep')
    finish
endif

" → Bringing :Ag up
nnoremap <leader>a :Rg<space>
