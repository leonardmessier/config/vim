if !empty(glob(".prettierrc.json")) || !empty(glob(".prettierrc"))
  autocmd BufWritePost *.ts exec 'CocCommand prettier.formatFile'
endif
