" Fugitive
" --------

" → git status
nnoremap <leader>gs :Git<CR>

" → git blame
nnoremap <leader>gb :Git blame<CR>

" → git commit
nnoremap <leader>gc :Git commit<CR>

" → git diff
nnoremap <leader>gd :Gvdiff<CR>

" → stage current file
nnoremap <leader>ga :Gwrite<CR>

" → current file history
nnoremap <leader>gl :Gclog<CR>

" -> fetch
nnoremap <leader>gf :Git fetch<CR>

" -> merge
nnoremap <leader>gm :Git merge

" -> push
nnoremap <leader>gp :Git push<CR>

" -> push force
nnoremap <leader>gP :Git push -f<CR>


" -> reset last commit
nnoremap <leader>gr :Git reset HEAD~1<CR>

" → Navigate to repository root
augroup nav_to_repo_root
    autocmd FileType netrw nnoremap <buffer> g~ :Gedit :/<CR>
augroup END

" GV
" --

nnoremap <leader>gv :GV<CR>
